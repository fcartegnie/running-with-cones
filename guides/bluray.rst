
.. Global macros

.. include:: /macros.rst

.. Specific macros

.. |MakeMKV| replace:: :program:`MakeMKV`


#####################################
  Read Blurays with VLC and MakeMKV
#####################################

Purpose
=======

This guide explains how to use the |MakeMKV| software with |VLC| to play your bluray discs on |Windows|.

Tested on :program:`Microsoft Windows 10`, with :program:`VLC 3.0.6` and :program:`MakeMKV Beta 1.14.3`.

Download MakeMKV
================

.. figure:: /images/guides/bluray/makemkv-website.png
   :width: 40%
   :align: center
   :target: https://www.makemkv.com

Go to the `MakeMKV official website <https://www.makemkv.com>`_, and download the installer.

Install MakeMKV
===============

.. figure:: /images/guides/bluray/makemkv-install.png
   :width: 40%
   :align: center

   Install MakeMKV

Install it on your preferred directory location.

The default provided proposed by the |MakeMKV| installer (:file:`C:\\Program Files (x86)\\MakeMKV`) is perfectly acceptable.

.. important::
   Be sure to **note** the |MakeMKV| install directory.

   It will be used afterwards.


Activate the MakeMKV license
============================

There are two different methods. Any of the two following are working (for now).

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/guides/bluray/makemkv-register.png
         :width: 40%

         Register MakeMKV

      Purchase a license and register it

   .. container:: descr

      .. figure:: /images/guides/bluray/makemkv-use1.png
         :width: 40%

         Use MakeMKV one time

      Activate the included evaluation license by launching |MakeMKV| software and using it one time.

.. note::
   As long as |MakeMKV| is in beta stage, the |MakeMKV| developer team is providing a free temporary license key.

   See `this post <https://www.makemkv.com/forum/viewtopic.php?t=1053>`_ for more details.


Get VLC
=======

Get the latest stable version of |VLC| on the `Official VLC Download page <https://www.videolan.org/vlc/index.html>`_ [#1]_.

The default provided proposed by the |VLC| installer (:file:`C:\\Program Files\\VideoLAN\\VLC`) is perfectly acceptable.

.. important::
   Be sure to also **note** the |VLC| install directory.

   It will be used afterwards.



Time to get our hands dirty
===========================

Copy the following library :file:`libmmbd.dll` file from the |MakeMKV| install directory to **VLC install directory**.

If you have installed the |VLC| **32bit** (win32) version
   Copy the :file:`libmmbd.dll` file from the |MakeMKV| install directory to the |VLC| install directory.

If you have installed the |VLC| **64bit** (win64) version
   Copy the :file:`libmmbd64.dll` file from the |MakeMKV| install directory to the |VLC| install directory.

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/guides/bluray/makemkv-libcopy.png
         :height: 200px

      Copy library from |MakeMKV| install directory...

   .. container:: descr

      .. figure:: /images/guides/bluray/makemkv-libcopy-dest.png
         :height: 200px

      ...Then paste the library to the |VLC| install directory.


Enjoy your awesome show
=======================

Launch |VLC| and select The Bluray disc


.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/guides/bluray/vlc-select-disk.png
         :height: 200px

      Select the VLC :menuselection:`Media --> Open Disc...` ...

   .. container:: descr

      .. figure:: /images/guides/bluray/vlc-select-bd.png
         :height: 200px

      ...Then select :guilabel:`bluray` in the :guilabel:`Disc` menu.

And... **Voilà !**.


On menus & java
===============

.. note::

   You need :program:`java jre` installed on your system to have the bluray menus supported on |VLC|.

   **BUT**, if you have a Windows **64bit** version, you need to install :program:`java jre` **64bit**, and not the standard (32bit) version.

   To download the 64bit version of :program:`java jre`, follow `This link <https://www.java.com/en/download/windows-64bit.jsp>`_.


.. hint::

   If you have problem with menus or java support on your system, you can still play your bluray disc with VLC in ``no menu`` mode.

   To do that, just click on the :guilabel:`No disc menus` checkbox in the VLC :guilabel:`Disc` menu.

   .. figure:: /images/guides/bluray/vlc-select-bd.png
      :height: 200px
      
      Click on the :guilabel:`No disc menus` checkbox below the disc type selection.


.. rubric:: Footnotes

.. [#1] What else?
